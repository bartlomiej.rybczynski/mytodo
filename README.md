# MyToDo

My TODO list implementation

# Main Features

It was created for purposes of bulding (physical) house by me. I suppose, most of the actions can be easily achieved with Excel, Google Docs or any other similiar tools.  It will give you opportunity to create a [multiple] list(s) in which you can add item(s). Those items can be selected/clicked and new details page will be displayed along with details related to it. Example: List name "School". Item name "Do a homework". Details for "Do a homework": "read pages from 5-15 from history book and prepare a list of dates mentioned there". It is planned to make it available from internet, synchornizing through google drive/icloud and sharing these list with other users. Login to application is not neccessary, all infrastructure is held in your cloud drive or device.

#Contributors
rybczyns ( Bartlomiej.Rybczynski [at] gmail.com)
